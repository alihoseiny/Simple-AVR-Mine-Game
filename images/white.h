/****************************************************************************
Image data created by the LCD Vision V1.05 font & image editor/converter
(C) Copyright 2011-2013 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Graphic LCD controller: KS0108 128x64 CS1,CS2
Image width: 8 pixels
Image height: 8 pixels
Color depth: 1 bits/pixel
Imported image file name: white.bmp

Exported monochrome image data size:
12 bytes for displays organized as horizontal rows of bytes
12 bytes for displays organized as rows of vertical bytes.
****************************************************************************/

#ifndef _WHITE_INCLUDED_
#define _WHITE_INCLUDED_

extern flash unsigned char white[];

#endif

