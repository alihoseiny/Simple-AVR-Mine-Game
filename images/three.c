/****************************************************************************
Image data created by the LCD Vision V1.05 font & image editor/converter
(C) Copyright 2011-2013 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Graphic LCD controller: KS0108 128x64 CS1,CS2
Image width: 16 pixels
Image height: 16 pixels
Color depth: 1 bits/pixel
Imported image file name: three.gif

Exported monochrome image data size:
36 bytes for displays organized as horizontal rows of bytes
36 bytes for displays organized as rows of vertical bytes.
****************************************************************************/

flash unsigned char three[]=
{
/* Image width: 16 pixels */
0x10, 0x00,
/* Image height: 16 pixels */
0x10, 0x00,
#ifndef _GLCD_DATA_BYTEY_
/* Image data for monochrome displays organized
   as horizontal rows of bytes */
0xFF, 0xFF, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 
0xF1, 0x8F, 0xF1, 0x8F, 0x01, 0x8E, 0xF1, 0x8F, 
0xF1, 0x8F, 0xF1, 0x8F, 0x01, 0x8E, 0xF1, 0x8F, 
0xF1, 0x8F, 0x01, 0x80, 0x01, 0x80, 0xFF, 0xFF, 
#else
/* Image data for monochrome displays organized
   as rows of vertical bytes */
0xFF, 0x01, 0x01, 0x01, 0xB1, 0xB1, 0xB1, 0xB1, 
0xB1, 0xF1, 0xF1, 0xF1, 0x01, 0x01, 0x01, 0xFF, 
0xFF, 0x80, 0x80, 0x80, 0x9B, 0x9B, 0x9B, 0x9B, 
0x9B, 0x9F, 0x9F, 0x9F, 0x80, 0x80, 0x80, 0xFF, 
#endif
};

