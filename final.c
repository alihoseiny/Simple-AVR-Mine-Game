	/*******************************************************
	This program was created by the
	CodeWizardAVR V3.12 Advanced
	Automatic Program Generator
	� Copyright 1998-2014 Pavel Haiduc, HP InfoTech s.r.l.
	http://www.hpinfotech.com

	Project :
	Version :
	Date    : 1/26/2018
	Author  :
	Company :
	Comments:


	Chip type               : ATmega32
	Program type            : Application
	AVR Core Clock frequency: 1.000000 MHz
	Memory model            : Small
	External RAM size       : 0
	Data Stack size         : 512
	*******************************************************/

	#include <mega32.h>
	#define TRUE 1
	#define FALSE 0
	#define BOMB 1
	#define CHOSEN 2
	#define ROWS 4
	#define COLUMNS 8
	#define CELLSIZE 16
	#define BOMBSINEACHROW	1

	// Including the images files
	#include "images/zero.c"
	#include "images/one.c"
	#include "images/two.c"
	#include "images/three.c"
	#include "images/four.c"
	#include "images/five.c"
	#include "images/six.c"
	#include "images/seven.c"
	#include "images/eight.c"
	#include "images/nine.c"
	#include "images/black.c"
	#include "images/blank.c"
	// Graphic Display functions
	#include <glcd.h>

	// Font used for displaying text
	// on the graphic display
	#include <font5x7.h>
	#include <stdint.h>
	#include <stdlib.h>
	#include <delay.h>



	unsigned char table_show[ROWS][COLUMNS] = {0};		// Stores the numbers those should show in each cell
	unsigned char table_status[ROWS][COLUMNS] = {0};	// Stores the status of each cell. has chosen or is a bomb.
	unsigned char currentX = 0;							// Current x position of the chooser square
	unsigned char currentY = 0;							// Current y position of the chooser square
	unsigned char numOfChosen = 0;						// Number of selected cells
	unsigned char gameIsOver = FALSE;					// The game is over or not
	flash unsigned char* images[] = {zero, one, two, three, four, five, six, seven, eight, nine};		// Stores images of numbers. Associated image for each digit is in the same index of that digit.

	// This function chooses current cell. If chosen cell is a bomb, the gameIsOver will set to TRUE. otherwise if this cell has not chosen before, set this as chsen and increases the number of numOfChosen;
	void choose(){
		if(table_status[currentY][currentX] == BOMB) gameIsOver = TRUE;
		else if(table_status[currentY][currentX] != CHOSEN){
			table_status[currentY][currentX] = CHOSEN;
			numOfChosen++;
		}
	}

	// sets current position at the right. If position is at the rightmost place, moves it at the leftmost.
	void moveRight(){
		if(currentX < COLUMNS - 1) currentX++;
		else{
			currentX = 0;
		}
	}

	// sets current position at the left. If position is at the rightmost place, moves it at the lefttmost.
	void moveLeft(){
		if(currentX > 0) currentX--;
		else{
			currentX = COLUMNS - 1;
		}
	}

	// sets current position at the top. If position is at the rightmost place, moves it at the bottom.
	void moveUp(){
		if(currentY > 0) currentY--;
		else{
			currentY = ROWS - 1;
		}
	}

	void moveDown(){
		if(currentY < ROWS - 1) currentY++;
		else{
			currentY = 0;
		}
	}

	// This function should call each time we need to draw the table of the game in the lcd. It loops throw the table cells and puts proper image of each cell to the screen.
	// For user pointer, puts a black image. for not chosen ones, puts blank image and for chosens, puts the image that contains the number of neighbor bombs.
	void drawTable(){
		int i, j;	// Loops counters.
		unsigned char write_x = 0, write_y = 0;		// The place of the lcd that should fill with a picture.
//		glcd_clear();
		for(i = 0; i < ROWS; i++){
			for(j = 0; j < COLUMNS; j++){
				if(i == currentY && j == currentX){
					glcd_putimagef(write_x, write_y, black, GLCD_PUTCOPY);
				}
				else if(table_status[i][j] == CHOSEN){
					glcd_putimagef(write_x, write_y, images[table_show[i][j]], GLCD_PUTCOPY);

				}
				else{
				glcd_putimagef(write_x, write_y, blank, GLCD_PUTCOPY);
				}
				write_x += CELLSIZE;
			}
			write_y += CELLSIZE;
			write_x = 0;
		}
	}

	// This function creates bombs in each row using c rand() function.
	void createBombs(){
		int i, j, rand_num;
		for(i = 0; i < ROWS; i++){
			for(j = 0; j < BOMBSINEACHROW; j++){
			rand_num = rand() % COLUMNS;
			while(table_status[i][rand_num] == BOMB){
					rand_num = rand() % COLUMNS;
			}
			table_status[i][rand_num] = BOMB;
			table_show[i][rand_num] = -1;
			}
		}
	}

	// Checks if user has won
	unsigned char hasWon(){
		return (BOMBSINEACHROW * ROWS + numOfChosen == ROWS * COLUMNS) && !gameIsOver? TRUE:FALSE;
	}

	// At the starting point, this function will call and creates the table, bombs and numbers of cells.
	void createGame(){
		int i, j;
		createBombs();
		for(i = 0; i < ROWS; i++){
			for(j = 0; j < COLUMNS; j++){
				if(table_status[i][j] == BOMB){
					if(j > 0 && table_show[i][j - 1] != -1){	// Cell is not in the leftmost place of the table
						table_show[i][j - 1] += 1;		// Increase the number of the right cell
						if(i > 0 && table_show[i - 1][j - 1] != -1){		// The cell is not in the topmost place in the table
							table_show[i - 1][j - 1] += 1;
						}
						if(i < ROWS - 1 && table_show[i + 1][j - 1] != -1){		// The cell is not in the bottom of the table
							table_show[i + 1][j - 1] += 1;
						}
					}
					if(j < COLUMNS - 1 && table_show[i][j + 1] != -1){ // Cell is not in the rightmost place of the table
						table_show[i][j + 1] += 1;
						if(i > 0 && table_show[i - 1][j + 1] != -1){	// The cell is not in the topmost place in the table
							table_show[i - 1][j + 1] += 1;
						}
						if(i < ROWS - 1 && table_show[i + 1][j + 1] != -1){		// The cell is not in the bottom of the table
							table_show[i + 1][j + 1] += 1;
						}
					}

					if(i != 0 && table_show[i - 1][j] != -1){		//Cell is not at the topmost polace of the table
							table_show[i - 1][j] += 1;
					}
					if( i != ROWS - 1 && table_show[i + 1][j] != -1){
						table_show[i + 1][j] += 1;
					}
				}
			}
		}
	}

	void main(void)
	{
	// Declare your local variables here
	// Variable used to store graphic display
	// controller initialization data
	GLCDINIT_t glcd_init_data;

	// Input/Output Ports initialization
	// Port A initialization
	// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In
	// Function: Bit7=Out Bit6=Out Bit5=Out Bit4=Out Bit3=Out Bit2=Out Bit1=Out Bit0=Out
	DDRA=(1<<DDA7) | (1<<DDA6) | (1<<DDA5) | (1<<DDA4) | (1<<DDA3) | (1<<DDA2) | (1<<DDA1) | (1<<DDA0);
	// State: Bit7=0 Bit6=0 Bit5=0 Bit4=0 Bit3=0 Bit2=0 Bit1=0 Bit0=0
	PORTA=(0<<PORTA7) | (0<<PORTA6) | (0<<PORTA5) | (0<<PORTA4) | (0<<PORTA3) | (0<<PORTA2) | (0<<PORTA1) | (0<<PORTA0);
	// Port B initialization
	// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=Out Bit2=Out Bit1=Out Bit0=Out
	DDRB=(0<<DDB7) | (0<<DDB6) | (0<<DDB5) | (0<<DDB4) | (1<<DDB3) | (1<<DDB2) | (1<<DDB1) | (1<<DDB0);
	// State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T
	PORTB=(0<<PORTB7) | (0<<PORTB6) | (0<<PORTB5) | (0<<PORTB4) | (0<<PORTB3) | (0<<PORTB2) | (0<<PORTB1) | (0<<PORTB0);

	// Port C initialization
	// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In
	DDRC=(0<<DDC7) | (0<<DDC6) | (0<<DDC5) | (0<<DDC4) | (0<<DDC3) | (0<<DDC2) | (0<<DDC1) | (0<<DDC0);
	// State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T
	PORTC=(1<<PORTC7) | (1<<PORTC6) | (1<<PORTC5) | (1<<PORTC4) | (1<<PORTC3) | (1<<PORTC2) | (1<<PORTC1) | (1<<PORTC0);

	// Port D initialization
	// Function: Bit7=In Bit6=In Bit5=In Bit4=In Bit3=In Bit2=In Bit1=In Bit0=In
	DDRD=(0<<DDD7) | (0<<DDD6) | (0<<DDD5) | (0<<DDD4) | (0<<DDD3) | (0<<DDD2) | (0<<DDD1) | (0<<DDD0);
	// State: Bit7=T Bit6=T Bit5=T Bit4=T Bit3=T Bit2=T Bit1=T Bit0=T
	PORTD=(0<<PORTD7) | (0<<PORTD6) | (0<<PORTD5) | (0<<PORTD4) | (0<<PORTD3) | (0<<PORTD2) | (0<<PORTD1) | (0<<PORTD0);

	// Timer/Counter 0 initialization
	// Clock source: System Clock
	// Clock value: Timer 0 Stopped
	// Mode: Normal top=0xFF
	// OC0 output: Disconnected
	TCCR0=(0<<WGM00) | (0<<COM01) | (0<<COM00) | (0<<WGM01) | (0<<CS02) | (0<<CS01) | (0<<CS00);
	TCNT0=0x00;
	OCR0=0x00;

	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: Timer1 Stopped
	// Mode: Normal top=0xFFFF
	// OC1A output: Disconnected
	// OC1B output: Disconnected
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer1 Overflow Interrupt: Off
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: Off
	// Compare B Match Interrupt: Off
	TCCR1A=(0<<COM1A1) | (0<<COM1A0) | (0<<COM1B1) | (0<<COM1B0) | (0<<WGM11) | (0<<WGM10);
	TCCR1B=(0<<ICNC1) | (0<<ICES1) | (0<<WGM13) | (0<<WGM12) | (0<<CS12) | (0<<CS11) | (0<<CS10);
	TCNT1H=0x00;
	TCNT1L=0x00;
	ICR1H=0x00;
	ICR1L=0x00;
	OCR1AH=0x00;
	OCR1AL=0x00;
	OCR1BH=0x00;
	OCR1BL=0x00;

	// Timer/Counter 2 initialization
	// Clock source: System Clock
	// Clock value: Timer2 Stopped
	// Mode: Normal top=0xFF
	// OC2 output: Disconnected
	ASSR=0<<AS2;
	TCCR2=(0<<PWM2) | (0<<COM21) | (0<<COM20) | (0<<CTC2) | (0<<CS22) | (0<<CS21) | (0<<CS20);
	TCNT2=0x00;
	OCR2=0x00;

	// Timer(s)/Counter(s) Interrupt(s) initialization
	TIMSK=(0<<OCIE2) | (0<<TOIE2) | (0<<TICIE1) | (0<<OCIE1A) | (0<<OCIE1B) | (0<<TOIE1) | (0<<OCIE0) | (0<<TOIE0);

	// External Interrupt(s) initialization
	// INT0: Off
	// INT1: Off
	// INT2: Off
	MCUCR=(0<<ISC11) | (0<<ISC10) | (0<<ISC01) | (0<<ISC00);
	MCUCSR=(0<<ISC2);

	// USART initialization
	// USART disabled
	UCSRB=(0<<RXCIE) | (0<<TXCIE) | (0<<UDRIE) | (0<<RXEN) | (0<<TXEN) | (0<<UCSZ2) | (0<<RXB8) | (0<<TXB8);

	// Analog Comparator initialization
	// Analog Comparator: Off
	// The Analog Comparator's positive input is
	// connected to the AIN0 pin
	// The Analog Comparator's negative input is
	// connected to the AIN1 pin
	ACSR=(1<<ACD) | (0<<ACBG) | (0<<ACO) | (0<<ACI) | (0<<ACIE) | (0<<ACIC) | (0<<ACIS1) | (0<<ACIS0);
	SFIOR=(0<<ACME);

	// ADC initialization
	// ADC disabled
	ADCSRA=(0<<ADEN) | (0<<ADSC) | (0<<ADATE) | (0<<ADIF) | (0<<ADIE) | (0<<ADPS2) | (0<<ADPS1) | (0<<ADPS0);

	// SPI initialization
	// SPI disabled
	SPCR=(0<<SPIE) | (0<<SPE) | (0<<DORD) | (0<<MSTR) | (0<<CPOL) | (0<<CPHA) | (0<<SPR1) | (0<<SPR0);

	// TWI initialization
	// TWI disabled
	TWCR=(0<<TWEA) | (0<<TWSTA) | (0<<TWSTO) | (0<<TWEN) | (0<<TWIE);

	// Graphic Display Controller initialization
	// The KS0108 connections are specified in the
	// Project|Configure|C Compiler|Libraries|Graphic Display menu:
	// DB0 - PORTA Bit 0
	// DB1 - PORTA Bit 1
	// DB2 - PORTA Bit 2
	// DB3 - PORTA Bit 3
	// DB4 - PORTA Bit 4
	// DB5 - PORTA Bit 5
	// DB6 - PORTA Bit 6
	// DB7 - PORTA Bit 7
	// E - PORTB Bit 0
	// RD /WR - PORTB Bit 1
	// RS - PORTB Bit 2
	// /RST - PORTB Bit 3
	// CS1 - PORTB Bit 4
	// CS2 - PORTB Bit 5

	// Specify the current font for displaying text
	glcd_init_data.font=font5x7;
	// No function is used for reading
	// image data from external memory
	glcd_init_data.readxmem=NULL;
	// No function is used for writing
	// image data to external memory
	glcd_init_data.writexmem=NULL;

	glcd_init(&glcd_init_data);
	createGame();
	drawTable();

	while (1)
		  {
			if(hasWon()){
				glcd_clear();
				glcd_outtextf("Hooooray\nYou're a winner");
				while(TRUE);
			}
			if(gameIsOver == FALSE){
				if(!PINC.0){	// UP Button
					moveUp();
					drawTable();
					delay_ms(1);
					while(!PINC.0);
				}
				else if(!PINC.1){	// Right Button
					moveRight();
					drawTable();
					delay_ms(1);
					while(!PINC.1);
				}
				else if(!PINC.2){ // Down Button
					moveDown();
					drawTable();
					delay_ms(1);
					while(!PINC.2);
				}
				else if(!PINC.3){	// Left Button
					moveLeft();
					drawTable();
					delay_ms(1);
					while(!PINC.3);
				}
				else if(!PINC.4){	// Choose
					choose();
					drawTable();
					delay_ms(1);
					while(!PINC.4);
				}

			}
			else if(gameIsOver){
				glcd_clear();
				glcd_outtextf("Game Over\nYou're a Loser");
				while(TRUE);
			}

          }
    }
